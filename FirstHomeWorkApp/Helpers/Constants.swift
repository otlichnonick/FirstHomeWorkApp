//
//  Constants.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 01.06.2022.
//

import Foundation

enum Constants {
    static let homeItem: String = "house"
    static let listItem: String = "list.bullet"
    static let settingsItem: String = "slider.vertical.3"
}
