//
//  CountryDetailScreen.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 01.06.2022.
//

import SwiftUI

struct CountryDetailScreen: View {
    var country: Country
    
    var body: some View {
        VStack(spacing: 20) {
            Text(country.name)
                .font(.title)
                .padding()
                .foregroundColor(.black)
                .background(Color.gray.opacity(0.5))
                .cornerRadius(16)
                .padding(.bottom, 30)
            
            DetailInfoRowView(title: "ВВП на душу населения, $", dataInfo: country.gdp.description)
            
            DetailInfoRowView(title: "Население, млн. человек", dataInfo: country.population.description)
            
            Spacer()
        }
        .padding()
    }
}

struct CountryDetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        CountryDetailScreen(country: Country(name: "Россия", gdp: 43000, population: 144))
    }
}

