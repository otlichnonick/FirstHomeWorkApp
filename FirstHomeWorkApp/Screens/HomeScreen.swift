//
//  HomeScreen.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 01.06.2022.
//

import SwiftUI

struct HomeScreen: View {
    @EnvironmentObject var router: Router
    @Binding var selection: Int
    
    var body: some View {
        ZStack {
            Image("G20")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea(.container, edges: [.top, .leading, .trailing])
            
            VStack(spacing: 20) {
                Spacer()
                
                CustomButton(callback: {
                    selection = 1
                }, title: "Посмотреть список стран")

                
                Spacer()

                CustomButton(callback: {
                    selection = 1
                    router.showDetailScreen = true
                }, title: "Посмотреть детальную информацию")
                
                Spacer()
            }
        }
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreen(selection: .constant(1))
    }
}
