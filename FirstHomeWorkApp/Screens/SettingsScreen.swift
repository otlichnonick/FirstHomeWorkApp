//
//  SettingsScreen.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 01.06.2022.
//

import SwiftUI

struct SettingsScreen: View {
    @State private var showSheet: Bool = false
    @State private var iconName: String = "🇷🇺"
    
    var body: some View {
        VStack {
            HStack {
                Button {
                    showSheet.toggle()
                } label: {
                    Text("Выберите язык")
                        .foregroundColor(.black)
                        .font(.title)
                }
                
                Spacer()
                
                Text(iconName)
                    .font(.title)
            }
            .actionSheet(isPresented: $showSheet) {
                ActionSheet(title: Text("Выберите язык"), buttons: [
                    .default(Text("Русский"), action: {
                        iconName = "🇷🇺"
                    }),
                    .default(Text("Итальянский"), action: {
                        iconName = "🇮🇹"
                    }),
                    .default(Text("Английский"), action: {
                        iconName = "🇬🇧"
                    }),
                    .cancel()
                ])
            }
            
            Spacer()
        }
        .navigationTitle(Text("Настройки"))
        .padding()

    }
}

struct SettingsScreen_Previews: PreviewProvider {
    static var previews: some View {
        SettingsScreen()
    }
}
