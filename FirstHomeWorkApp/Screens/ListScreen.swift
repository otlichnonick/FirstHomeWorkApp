//
//  ListScreen.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 01.06.2022.
//

import SwiftUI

struct ListScreen: View {
    @EnvironmentObject var router: Router
    @State private var selectedCountry: Country = Country.list.first ?? Country()
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(Country.list) { country in
                        Button {
                            router.showDetailScreen = true
                            selectedCountry = country
                        } label: {
                            NavigationLink(country.name,
                                           isActive: $router.showDetailScreen) {
                                router.showDetailViewAbout(country: selectedCountry)
                            }
                        }
                    }
                    
                    NavigationLink {
                        router.showWebView()
                    } label: {
                        Text("Посмотреть весь список")
                            .font(.title2)
                            .foregroundColor(.accentColor)
                    }
                }
            }
            .navigationTitle("Страны G20")
        }
    }
}

struct ListScreen_Previews: PreviewProvider {
    static var previews: some View {
        ListScreen()
    }
}

struct Country: Identifiable {
    let id = UUID()
    var name: String = ""
    var gdp: Int = 0
    var population: Int = 0
    
    static let list: [Country] = [
        Country(name: "США", gdp: 57467, population: 323),
        Country(name: "Саудовская Аравия", gdp: 54431, population: 32),
        Country(name: "Германия", gdp: 48730, population: 83),
        Country(name: "Австралия", gdp: 46790, population: 24),
        Country(name: "Канада", gdp: 44025, population: 36),
        Country(name: "Великобритания", gdp: 42609, population: 66),
        Country(name: "Япония", gdp: 41470, population: 127),
        Country(name: "Франция", gdp: 41466, population: 67),
        Country(name: "Италия", gdp: 38161, population: 61),
        Country(name: "Южная корея", gdp: 35751, population: 25),
        Country(name: "Турция", gdp: 24244, population: 80),
        Country(name: "Россия", gdp: 23163, population: 144)
    ]
}
