//
//  WebViewScreen.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 03.06.2022.
//

import SwiftUI

struct WebViewScreen: View {
    var body: some View {
        CustomUIView()
    }
}

struct WebViewScreen_Previews: PreviewProvider {
    static var previews: some View {
        WebViewScreen()
    }
}
