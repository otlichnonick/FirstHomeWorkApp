//
//  ContentView.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 01.06.2022.
//

import SwiftUI

struct ContentView: View {
    @State private var selection: Int = 0
    
    var body: some View {
        TabView(selection: $selection) {
            HomeScreen(selection: $selection)
                .tag(0)
                .tabItem {
                    Image(systemName: Constants.homeItem)
                }
            
            ListScreen()
                .tag(1)
                .tabItem {
                    Image(systemName: Constants.listItem)
                }
            
            SettingsScreen()
                .tag(2)
                .tabItem {
                    Image(systemName: Constants.settingsItem)
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
