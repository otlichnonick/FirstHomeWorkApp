//
//  CustomButton.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 03.06.2022.
//

import SwiftUI

struct CustomButton: View {
    let callback: () -> Void
    let title: String
    
    var body: some View {
        Button {
            callback()
        } label: {
            Text(title)
                .multilineTextAlignment(.center)
                .font(.title)
                .foregroundColor(.yellow)
        }
        .padding(8)
        .background(Color.indigo)
        .cornerRadius(16)
        .frame(width: UIScreen.main.bounds.width)
    }
}

struct CustomButton_Previews: PreviewProvider {
    static var previews: some View {
        CustomButton(callback: {}, title: "hello")
    }
}
