//
//  DetailInfoRowView.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 03.06.2022.
//

import SwiftUI

struct DetailInfoRowView<DataInfo: StringProtocol>: View {
    let title: String
    let dataInfo: DataInfo
    
    var body: some View {
    HStack {
        Text(title)
        
        Spacer()
        
        Text(dataInfo)
    }
    }
}

struct DetailInfoRowView_Previews: PreviewProvider {
    static var previews: some View {
        DetailInfoRowView(title: "item", dataInfo: 0.description)
    }
}
