//
//  CustomUIView.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 03.06.2022.
//

import UIKit
import SwiftUI
import WebKit

struct CustomUIView: UIViewRepresentable {
    typealias UIViewType = WKWebView
    var url = URL(string: "https://ru.wikipedia.org/wiki/%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F_%D0%B4%D0%B2%D0%B0%D0%B4%D1%86%D0%B0%D1%82%D0%BA%D0%B0")
    
    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true
        
        if let url = url {
        let request = URLRequest(url: url)
            webView.load(request)
        }
        
        return webView
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        
    }
    
    
    
    
    
}
