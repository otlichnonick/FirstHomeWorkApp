//
//  Router.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 03.06.2022.
//

import Foundation
import SwiftUI

final class Router: ObservableObject {
    @Published var showDetailScreen = false
    
    func showDetailViewAbout(country: Country) -> some View {
        CountryDetailScreen(country: country)
    }
    
    func showWebView() -> some View {
        CustomUIView()
    }
}
