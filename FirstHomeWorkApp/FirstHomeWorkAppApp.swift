//
//  FirstHomeWorkAppApp.swift
//  FirstHomeWorkApp
//
//  Created by Anton Agafonov on 01.06.2022.
//

import SwiftUI

@main
struct FirstHomeWorkAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(Router())
        }
    }
}
